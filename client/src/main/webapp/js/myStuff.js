var jWebSocketClient = null;

function initWebSocket() {
    if (jws.browserSupportsWebSockets()) {
        jWebSocketClient = new jws.jWebSocketJSONClient();
    } else {
        var lMsg = jws.MSG_WS_NOT_SUPPORTED;
    }
}

function logon() {
    //var lRes = jWebSocketClient.logon("ws://10.0.0.136:8787", "username", "password", {
        var lRes = jWebSocketClient.logon("ws://10.0.0.138:8787", "username", "password", {

// OnOpen callback
        OnOpen: function(aEvent) {
            document.getElementById("log").innerHTML += "jWebSocket connection established.";
        },
// OnMessage callback
        OnMessage: function(aEvent, aToken) {
            document.getElementById("log").innerHTML += "<p>jWebSocket '" + aToken.type + "' token received, full message: '"
                    + aEvent.data + "</p>";
        },
// OnClose callback
        OnClose: function(aEvent) {
            document.getElementById("log").innerHTML += "<p>jWebSocket connection closed.</p>";
        }
    });
}

function sendtoken() {
    if (jWebSocketClient.isConnected()) {
         document.getElementById("log").innerHTML += "sending message to jwebsocket";
        var lToken = {
            ns: "nl.xebia.frmsocket",
            type: "getInfo"
        };
        jWebSocketClient.sendToken(lToken, {
            OnResponse: function(aToken) {
                document.getElementById("log").innerHTML += "<p>Server responded: "
                        + "vendor: " + aToken.vendor
                        + ", version: " + aToken.version + "</p";
            }
        });
    } else {
        document.getElementById("log").innerHTML += "Not connected.";
    }
}