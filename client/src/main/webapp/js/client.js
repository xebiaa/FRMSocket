var socket;

$(document).ready(function() {
  $('#messageInput').keyup(function(event) {
		if (event.keyCode == 13) {
			sendMessage($("#messageInput").val());
		}
    });
});


if(!("WebSocket" in window)){
    alert('gebruik chrome!');
}else{
    connect();
}




    function connect(){
    try{
	    var host = "ws://10.0.0.136:8787";
        socket = new WebSocket(host);

        socket.onopen = function(){
       		 $("<span/>").text("connected").appendTo("#messages");
        }

        socket.onmessage = function(msg){
       		 $("<span/>").text(msg.data).appendTo("#messages");
        }

        socket.onclose = function(){
            $("<span/>").text("connection closed").appendTo("#messages");
        }

    } catch(exception){
   		 alert(exception);
    }
}

function sendMessage(message) {
    socket.send(message);
}