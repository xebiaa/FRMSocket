package com.xebia.frmsocket.server;

import org.jwebsocket.kit.RawPacket;
import org.jwebsocket.kit.WebSocketServerEvent;

/** @author mischa */
public class RequestPacketRunner implements Runnable {
    private WebSocketServerEvent event;
    private String resource;
    private boolean running = false;

    public RequestPacketRunner(WebSocketServerEvent event, String resource) {
        this.event = event;
        this.resource = resource;
    }

    /**
     * Gets the event.
     * @return event The event.
     */
    public WebSocketServerEvent getEvent() {
        return event;
    }

    @Override
    public void run() {
        running = true;
        while (running) {
            try {
                Thread.sleep(1000);
                event.sendPacket(new RawPacket(resource));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
}
