package com.xebia.frmsocket.server;

import org.jwebsocket.listener.WebSocketServerTokenEvent;
import org.jwebsocket.token.JSONToken;

/** @author mischa */
public class RequestTokenRunner implements Runnable {
    private WebSocketServerTokenEvent event;
    private String resource;
    private boolean running = false;

    public RequestTokenRunner(WebSocketServerTokenEvent event, String resource) {
        this.event = event;
        this.resource = resource;
    }

    /**
     * Gets the event.
     * @return event The event.
     */
    public WebSocketServerTokenEvent getEvent() {
        return event;
    }

    @Override
    public void run() {
        running = true;
        while (running) {
            try {
                Thread.sleep(1000);
                event.sendToken(new JSONToken(resource));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
}
