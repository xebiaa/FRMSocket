/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xebia.frmsocket.server;

import org.apache.log4j.Logger;
import org.jwebsocket.api.WebSocketPacket;
import org.jwebsocket.appserver.WebSocketHttpSessionMerger;
import org.jwebsocket.factory.JWebSocketFactory;
import org.jwebsocket.kit.WebSocketServerEvent;
import org.jwebsocket.listener.WebSocketServerTokenEvent;
import org.jwebsocket.listener.WebSocketServerTokenListener;
import org.jwebsocket.logging.Logging;
import org.jwebsocket.server.TokenServer;
import org.jwebsocket.token.JSONToken;
import org.jwebsocket.token.Token;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * FRMSocket
 * @author aschulze
 */
public class FRMSocketPlugin extends HttpServlet implements WebSocketServerTokenListener {
    private static Logger log = Logging.getLogger(FRMSocketPlugin.class);

    /** {@inheritDoc}. */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {
            out.println("This session: " + request.getSession().getId());
            out.println("Http sessions: " + WebSocketHttpSessionMerger.getHttpSessionsCSV());
            out.println("WebSocket sessions: " + WebSocketHttpSessionMerger.getWebSocketSessionsCSV());
        } finally {
            out.close();
        }
    }

    @Override
    public void init() {
        log.info("Adding servlet '" + getClass().getSimpleName() + "' to WebSocket listeners...");
        TokenServer lServer = (TokenServer) JWebSocketFactory.getServer("ts0");
        if (lServer != null) {
            lServer.addListener(this);
        }
    }

    @Override
    public void processOpened(WebSocketServerEvent aEvent) {
        log.info("Opened WebSocket session: " + aEvent.getSession().getSessionId());
        WebSocketHttpSessionMerger.addWebSocketSession(aEvent.getSession());
    }

    @Override
    public void processPacket(WebSocketServerEvent aEvent, WebSocketPacket aPacket) {
        log.info("Processing packet: " + aPacket.toString());
        FRMSocketManager.addRequestPacket(new RequestPacketRunner(aEvent, aPacket.getASCII()));
    }

    @Override
    public void processToken(WebSocketServerTokenEvent aEvent, Token aToken) {
        if (aToken.asMap().size() != 0) {
            log.info("Processing token: " + aToken.toString());
            FRMSocketManager.addRequestToken(new RequestTokenRunner(aEvent, aToken.toString()));
        }
    }

    @Override
    public void processClosed(WebSocketServerEvent aEvent) {
        log.info("Closed WebSocket session: " + aEvent.getSession().getSessionId());
        WebSocketHttpSessionMerger.removeWebSocketSession(aEvent.getSession());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }


}
