package com.xebia.frmsocket.server;

import java.util.HashMap;
import java.util.Map;

/** @author mischa */
public class FRMSocketManager {

    private static FRMSocketManager manager;
    private Map<String, RequestPacketRunner> packetRunners;
    private Map<String, RequestTokenRunner> tokenRunners;

    private FRMSocketManager() {
        packetRunners = new HashMap<String, RequestPacketRunner>();
        tokenRunners = new HashMap<String, RequestTokenRunner>();
    }

    public static FRMSocketManager instance() {
        if (manager == null) {
            synchronized (FRMSocketManager.class) {
                if (manager == null) {
                    manager = new FRMSocketManager();
                }
            }
        }
        return manager;
    }

    public static void addRequestPacket(RequestPacketRunner runner) {
        String sessionId = runner.getEvent().getSession().getSessionId();
        if(instance().packetRunners.containsKey(sessionId)) {
            instance().packetRunners.get(sessionId).setRunning(false);
        }
        instance().packetRunners.put(sessionId, runner);
        new Thread(runner).start();
    }

     public static void addRequestToken(RequestTokenRunner runner) {
        String sessionId = runner.getEvent().getSession().getSessionId();
        if(instance().tokenRunners.containsKey(sessionId)) {
            instance().tokenRunners.get(sessionId).setRunning(false);
        }
        instance().tokenRunners.put(sessionId, runner);
        new Thread(runner).start();
    }

}
