/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xebia.frmsocket.server;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.apache.log4j.Logger;
import org.jwebsocket.appserver.WebSocketHttpSessionMerger;
import org.jwebsocket.logging.Logging;

/**
 * Web application lifecycle listener.
 * Here the http session is added or removed respectively from the
 * global WebSocketHttpSessionMerger.
 */
public class SessionListener implements HttpSessionListener {
	private static Logger mLog = Logging.getLogger(SessionListener.class);

	@Override
	public void sessionCreated(HttpSessionEvent hse) {
		WebSocketHttpSessionMerger.addHttpSession(hse.getSession());
		mLog.info("Created Http session: '" + hse.getSession().getId() + "'");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent hse) {
		WebSocketHttpSessionMerger.removeHttpSession(hse.getSession());
		mLog.info("Destroyed Http session: '" + hse.getSession().getId() + "'");
	}
}
